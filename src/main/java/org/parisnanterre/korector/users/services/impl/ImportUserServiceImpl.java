package org.parisnanterre.korector.users.services.impl;

import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import org.parisnanterre.korector.users.exceptions.FileStorageException;
import org.parisnanterre.korector.users.payload.response.UserImportResponse;
import org.parisnanterre.korector.users.services.ImportUserService;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.List;
@Service
public class ImportUserServiceImpl implements ImportUserService {
    @Override
    public List<UserImportResponse> fileToUser(MultipartFile file) {
        try {
            if (file.isEmpty()) throw new FileStorageException("Sorry, file is empty");
            // On vérifie que le nom de fichier ne comprend pas de caractère bizarres (cd ../.. etc)
            if (file.getOriginalFilename() != null && StringUtils.cleanPath(file.getOriginalFilename()).contains("..")) throw new FileStorageException("Nom de fichier invalide");
            Reader reader = new BufferedReader(new InputStreamReader(file.getInputStream()));
            CsvToBean<UserImportResponse> csvToBean = new CsvToBeanBuilder(reader)
                    .withSeparator(';')
                    .withType(UserImportResponse.class)
                    .withIgnoreLeadingWhiteSpace(true)
                    .build();
            return csvToBean.parse();
        }catch (IOException ex){
            throw new FileStorageException("Echec de l'import de fichier", ex);
        }
    }
}
