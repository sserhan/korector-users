package org.parisnanterre.korector.users.services.impl;

import org.keycloak.admin.client.CreatedResponseUtil;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.admin.client.resource.UserResource;
import org.keycloak.representations.idm.CredentialRepresentation;
import org.keycloak.representations.idm.RoleRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.parisnanterre.korector.users.config.KeycloakConfig;
import org.parisnanterre.korector.users.services.UserService;
import org.passay.CharacterRule;
import org.passay.EnglishCharacterData;
import org.passay.PasswordGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

import static org.passay.DictionaryRule.ERROR_CODE;

/**
 *  Implémentation de UserService
 *  Cette classe utilise l'api keycloak admin
 *
 */

@Service
public class UserServiceImpl implements UserService {

    /**
     * Service récupérant les configurations du fichier de propriété .yml
     */
    private final KeycloakConfig keycloakConfig;

    /**
     * Service permettant d'envoyer des mails
     */
    public final JavaMailSender emailSender;


    public UserServiceImpl(KeycloakConfig keycloakConfig, JavaMailSender emailSender) {
        this.keycloakConfig = keycloakConfig;
        this.emailSender = emailSender;
    }

    /**
     * Renvoi la liste des utilisateurs sous la forme UserRepresentation de Keycloak
     *
     * @param bearerToken token jwt venant du client
     * @return liste des utilisateurs
     */
    @Override
    public List<UserRepresentation> all(String bearerToken) {
        List<UserRepresentation> users = new ArrayList<>();
        this.repository(bearerToken).users().list().forEach(user -> {
            // On charge les rôles à part (keyclock ne les load pas)
            user.setRealmRoles(loadUserRoles(bearerToken, user.getId()));
            users.add(user);
        });
        return users;
    }

    /**
     *  Renvoi un utilisateur
     *
     * @param id id de l'utilisateur
     * @param bearerToken token jwt venant du client
     * @return un utilisateur
     */
    @Override
    public UserRepresentation getOne(String id, String bearerToken) {
        return this.repository(bearerToken).users().get(id).toRepresentation();
    }

    /**
     * Supprime un utilisateur
     * @param id id de l'utilisateur
     * @param bearerToken token jwt venant du client
     */
    @Override
    public void delete(String id, String bearerToken) {
        this.repository(bearerToken).users().get(id).remove();
    }

    /**
     * Crée un utilisateur
     *
     *
     * @param userRepresentation userRepresentation sans mot de passe ni roles venant du UserForm converti
     * @param bearerToken token jwt venant du client
     * @param roles liste des roles
     * @return utilisateur
     */
    @Override
    public UserRepresentation save(UserRepresentation userRepresentation, String bearerToken, List<String> roles) {
        userRepresentation.setEnabled(true);
        Response response = this.repository(bearerToken).users().create(userRepresentation);
        String userId = CreatedResponseUtil.getCreatedId(response);
        // On créé le mot de passe et assigne les rôles séparément (keycloak nous l'oblige)
        updatePasswordAndRoles(roles,userId,bearerToken);
        UserRepresentation userResponse = this.repository(bearerToken).users().get(userId).toRepresentation();
        userResponse.setRealmRoles(loadUserRoles(userRepresentation.getId(), bearerToken));
        return userResponse;
    }

    /**
     * Modifie un utilisateur
     *
     * @param userRepresentation userRepresentation venant du UserForm converti
     * @param bearerToken token jwt venant du client
     * @return utilisateur
     */
    @Override
    public UserRepresentation update(UserRepresentation userRepresentation, String bearerToken, List<String> roles) {
        this.repository(bearerToken).users().get(userRepresentation.getId()).update(userRepresentation);
        UserResource resource = updateRoles(roles, userRepresentation.getId(), bearerToken, true);
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return this.getOne(userRepresentation.getId(),bearerToken);
    }

    /**
     *  Renvoi la liste des rôles du realm keycloak
     * @param bearerToken token jwt venant du client
     * @return roles
     */

    @Override
    public List<String> getRoles(String bearerToken) {
        List<String> roles = new ArrayList<>();
        this.repository(bearerToken).roles().list().forEach(x -> roles.add(x.getName()));
        return roles;
    }

    /**
     * Permet de nous connecter à keycloak
     * @param bearerToken
     * @return
     */

    private RealmResource repository(String bearerToken){
        return Keycloak.getInstance(keycloakConfig.getHost() + "/auth",
                keycloakConfig.getRealm(), keycloakConfig.getClient(), bearerToken).realm(keycloakConfig.getRealm());
    }

    /**
     * Créé le mot de passe et ajoute les rôles lors de la création et envoi un mail
     * @param roles liste de rôles
     * @param userId id de l'user
     * @param bearerToken token jwt venant du client
     */

    private void updatePasswordAndRoles(List<String> roles, String userId, String bearerToken){
        CredentialRepresentation credential = new CredentialRepresentation();
        credential.setTemporary(true);
        credential.setType(CredentialRepresentation.PASSWORD);
        String password = generatePassayPassword();
        credential.setValue(password);
        UserResource userResource = updateRoles(roles, userId, bearerToken, false);
        userResource.resetPassword(credential);
        this.repository(bearerToken).users().get(userId).update(userResource.toRepresentation());
        sendRegisterMail(userResource.toRepresentation().getEmail(), userResource.toRepresentation().getUsername(), password);
    }

    /**
     * Met à jour les rôles, un témoin est présent pour sauvegarder ou non dans cette méthode (permet de la réutiliser)
     * @param roles liste de roles
     * @param userId id de l'utilisateur
     * @param bearerToken token jwt venant du client
     * @param save témoin de sauvegarde
     * @return
     */
    private UserResource updateRoles(List<String> roles, String userId, String bearerToken, boolean save){
        UserResource userResource = this.repository(bearerToken).users().get(userId);
        List<RoleRepresentation> rolesRepresentation = new ArrayList<>();
        roles.forEach(x -> {
            rolesRepresentation.add(this.repository(bearerToken).roles().get(x).toRepresentation());
        });
        userResource.roles().realmLevel().add(rolesRepresentation);
        if(save) this.repository(bearerToken).users().get(userId).update(userResource.toRepresentation());
        return userResource;
    }

    /**
     * Permet d'envoyer le mail de création d'utilisateur
     * @param to email de l'user
     * @param username username de l'user
     * @param password mot de passe a envoyer
     */

    private void sendRegisterMail(String to,String username ,String password){
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(to);
        message.setSubject("Korector : Première connexion");
        message.setText("Votre enseignant vous a inscrit sur l'application korector " +
                "\nUsername: " + username + "\nPassword: " + password +
                "\nIl vous sera demandé de changer de mot de passe lors de votre première connexion" +
                "\nCordialement, " +
                "\n");
        emailSender.send(message);
    }

    /**
     *  Génère un mot de passe de qualité aléatoire
     * @return password
     */
    private String generatePassayPassword() {
        PasswordGenerator gen = new PasswordGenerator();
        EnglishCharacterData lowerCaseChars = EnglishCharacterData.LowerCase;
        CharacterRule lowerCaseRule = new CharacterRule(lowerCaseChars);
        lowerCaseRule.setNumberOfCharacters(2);

        EnglishCharacterData upperCaseChars = EnglishCharacterData.UpperCase;
        CharacterRule upperCaseRule = new CharacterRule(upperCaseChars);
        upperCaseRule.setNumberOfCharacters(2);
        EnglishCharacterData digitChars = EnglishCharacterData.Digit;
        CharacterRule digitRule = new CharacterRule(digitChars);
        digitRule.setNumberOfCharacters(2);
        CharacterRule splCharRule = new CharacterRule(new org.passay.CharacterData() {
            @Override
            public String getErrorCode() {
                return ERROR_CODE;
            }

            @Override
            public String getCharacters() {
                return "!@#$%^&*()_+";
            }
        });
        splCharRule.setNumberOfCharacters(2);
        return gen.generatePassword(10, splCharRule, lowerCaseRule,
                upperCaseRule, digitRule);
    }

    /**
     *  Recherche les realm rôles d'un utilisateur
     * @param bearerToken token jwt venant du client
     * @param userId id de l'utilisateur
     * @return liste des roles
     */
    private List<String> loadUserRoles(String bearerToken, String userId){
        List<String> roles = new ArrayList<>();
        this.repository(bearerToken)
                .users()
                .get(userId)
                .roles()
                .realmLevel()
                .listAll()
                .forEach(x -> roles.add(x.getName()));
        return roles;
    }

}
