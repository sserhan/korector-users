package org.parisnanterre.korector.users.services;


import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public interface UserService {

    /**
     * Renvoi la liste des utilisateurs sous la forme UserRepresentation de Keycloak
     *
     * @param bearerToken token jwt venant du client
     * @return liste des utilisateurs
     */
    List<UserRepresentation> all(String bearerToken);

    /**
     *  Renvoi un utilisateur
     *
     * @param id id de l'utilisateur
     * @param bearerToken token jwt venant du client
     * @return un utilisateur
     */
    UserRepresentation getOne(String id, String bearerToken);

    /**
     * Supprime un utilisateur
     * @param id id de l'utilisateur
     * @param bearerToken token jwt venant du client
     */
    void delete(String id, String bearerToken);

    /**
     * Crée un utilisateur
     *
     *
     * @param userRepresentation userRepresentation sans mot de passe ni roles venant du UserForm converti
     * @param bearerToken token jwt venant du client
     * @param roles liste des roles
     * @return utilisateur
     */
    UserRepresentation save(UserRepresentation userRepresentation, String bearerToken, List<String> roles);
    /**
     * Modifie un utilisateur
     *
     * @param userRepresentation userRepresentation venant du UserForm converti
     * @param bearerToken token jwt venant du client
     * @return utilisateur
     */
    UserRepresentation update(UserRepresentation userRepresentation, String bearerToken, List<String> roles);

    /**
     *  Renvoi la liste des rôles du realm keycloak
     * @param bearerToken token jwt venant du client
     * @return roles
     */
    List<String> getRoles(String bearerToken);
}
