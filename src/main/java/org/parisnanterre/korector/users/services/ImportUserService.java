package org.parisnanterre.korector.users.services;

import org.parisnanterre.korector.users.payload.response.UserImportResponse;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Service
public interface ImportUserService {
    List<UserImportResponse> fileToUser(MultipartFile file);
}
