package org.parisnanterre.korector.users.services;

import org.springframework.stereotype.Service;
import org.springframework.web.bind.MethodArgumentNotValidException;
import java.util.ArrayList;

@Service
public class ValidationExceptionsService {

    public ArrayList<String> getErrors(MethodArgumentNotValidException ex) {
        ArrayList<String> errors = new ArrayList<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            //String fieldName = ((FieldError) error).getField();
            errors.add(error.getDefaultMessage());
        });

        return errors;
    }
}
