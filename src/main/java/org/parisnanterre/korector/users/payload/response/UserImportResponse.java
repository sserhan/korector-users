package org.parisnanterre.korector.users.payload.response;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvBindByPosition;

import java.io.Serializable;

public class UserImportResponse {
    @CsvBindByName(column = "Username")
    private String username;
    @CsvBindByName(column = "Email")
    private String email;
    @CsvBindByName(column = "Nom")
    private String nom;
    @CsvBindByName(column = "Prenom")
    private String prenom;

    public UserImportResponse() {}

    public UserImportResponse(String username, String email, String nom, String prenom) {
        this.username = username;
        this.email = email;
        this.nom = nom;
        this.prenom = prenom;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }
}

