package org.parisnanterre.korector.users.payload;

import org.keycloak.representations.idm.UserRepresentation;
import org.parisnanterre.korector.users.payload.request.UserForm;
import org.parisnanterre.korector.users.payload.response.UserResponse;

import java.util.ArrayList;
import java.util.List;

public class ModelMapper {

    public static UserResponse userKeyCloakToUserResponse(UserRepresentation userRepresentation) {
        UserResponse user = new UserResponse();
        List<String> roles = new ArrayList<>();
        user.setId(userRepresentation.getId());
        user.setEmail(userRepresentation.getEmail());
        user.setNom(userRepresentation.getLastName());
        user.setPrenom(userRepresentation.getFirstName());
        user.setUsername(userRepresentation.getUsername());
        if(userRepresentation.getRealmRoles() != null)
        user.setRoles(userRepresentation.getRealmRoles());
        return user;
    }

    public static UserRepresentation userFormToUserRepresentation(UserForm userForm){
        UserRepresentation userRepresentation = new UserRepresentation();
        if(userForm.getId() != null) userRepresentation.setId(userForm.getId());
        userRepresentation.setEmail(userForm.getEmail());
        userRepresentation.setUsername(userForm.getUsername());
        userRepresentation.setFirstName(userForm.getPrenom());
        userRepresentation.setLastName(userForm.getNom());
        return userRepresentation;
    }
}
