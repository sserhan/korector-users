package org.parisnanterre.korector.users.api;

import org.parisnanterre.korector.users.payload.ModelMapper;
import org.parisnanterre.korector.users.payload.request.UserForm;
import org.parisnanterre.korector.users.payload.response.UserImportResponse;
import org.parisnanterre.korector.users.payload.response.UserResponse;
import org.parisnanterre.korector.users.services.ImportUserService;
import org.parisnanterre.korector.users.services.UserService;
import org.parisnanterre.korector.users.services.ValidationExceptionsService;
import org.springframework.http.*;


import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import javax.validation.Valid;
import java.util.*;

@RestController
@RequestMapping("/api/v1")
public class UserController {


    private final UserService userService;

    private final ImportUserService importUserService;

    public UserController(UserService userService, ImportUserService importUserService) {
        this.userService = userService;
        this.importUserService = importUserService;
    }



    @GetMapping("/users")
    public List<UserResponse> all(@RequestHeader("Authorization") String bearerToken){
        List<UserResponse> users = new ArrayList<>();
       this.userService.all(bearerToken)
                .forEach(user ->users.add(ModelMapper.userKeyCloakToUserResponse(user)));
        return users;
    }


    @GetMapping("/users/{id}")
    public UserResponse getOne(@PathVariable String id, @RequestHeader("Authorization") String bearerToken){
        return ModelMapper.userKeyCloakToUserResponse(this.userService.getOne(id, bearerToken));
    }

    @GetMapping("/users/roles")
    public List<String> roles(@RequestHeader("Authorization") String bearerToken){
        return userService.getRoles(bearerToken);
    }

    @PostMapping("/users")
    public UserResponse save(@Valid @RequestBody UserForm userForm, @RequestHeader("Authorization") String bearerToken){
        return ModelMapper.userKeyCloakToUserResponse(
                userService.save(
                        ModelMapper.userFormToUserRepresentation(userForm), bearerToken, userForm.getRoles()));
    }

    @DeleteMapping("/users/{id}")
    public void delete(@PathVariable String id, @RequestHeader("Authorization") String bearerToken){
        this.userService.delete(id, bearerToken);
    }

    @PutMapping("/users")
    public UserResponse update(@Valid @RequestBody UserForm userForm, @RequestHeader("Authorization") String bearerToken){
        return ModelMapper.userKeyCloakToUserResponse(
                userService.update(
                        ModelMapper.userFormToUserRepresentation(userForm), bearerToken, userForm.getRoles()
                ));
    }

    @PostMapping("/users/import")
    public List<UserImportResponse> uploadFile(@RequestParam("file")MultipartFile file){
        return importUserService.fileToUser(file);
    }

}
